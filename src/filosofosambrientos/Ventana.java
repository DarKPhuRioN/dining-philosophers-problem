package filosofosambrientos;

import Base.Comedor;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import net.miginfocom.swing.MigLayout;

/* @author Julio A. Vásquez L. */
public class Ventana extends JFrame implements ActionListener {

    public JTable jtEstados;
    private Thread trProgreso;
    private Comedor cenar;
    private final JButton play = new JButton("Play");
    private final JButton stop = new JButton("Stop");
    private final JButton pause = new JButton("Pause");

    public Ventana() {
        super.setTitle("Cena de los Filósofos");
        super.setSize(790, 650);
        super.setResizable(false);
        super.setLayout(new BorderLayout());
        super.setDefaultCloseOperation(EXIT_ON_CLOSE);
        panel();
        super.setVisible(true);
    }

    private void panel() {
        icono();
        JPanel jpprincipalNort = new JPanel(new MigLayout("", "[right]"));//derecha
        jpprincipalNort.setBackground(Color.black);
        jpprincipalNort.setBorder(BorderFactory.createEtchedBorder());
        JPanel jpIzq = new JPanel(new MigLayout("", "[right]"));
        jpIzq.setBorder(BorderFactory.createTitledBorder("Ajustes"));
        jpIzq.setBackground(Color.white);
        jpprincipalNort.add(jpIzq, "growx,growy");
        jpIzq.add(new JLabel("Nº de Filosofos :"), "gaptop 10");//espacio arriba
        Object lsNfilosofos[] = {"5", "10", "15"};
        JComboBox jcNfilosofos = new JComboBox(lsNfilosofos);
        jpIzq.add(jcNfilosofos, "growx,wrap");
        jpIzq.add(new JLabel("Tipo de Filosofos :"), "gapbottom 6");
        Object lsTipo[] = {"Antiguos", "Modernos"};
        JComboBox jcTipo = new JComboBox(lsTipo);
        jpIzq.add(jcTipo, "growx,wrap,align left");
        JPanel jpCen = new JPanel(new MigLayout("", "[left]"));
        jpCen.setBackground(Color.white);
        jpCen.setBorder(BorderFactory.createTitledBorder("Control"));
        jpprincipalNort.add(jpCen, "growx,growy");
        play.addActionListener(this);
        play.setForeground(Color.white);
        play.setBackground(Color.red);
        jpCen.add(play);
        stop.addActionListener(this);
        stop.setForeground(Color.white);
        stop.setBackground(Color.red);
        jpCen.add(stop);
        pause.addActionListener(this);
        pause.setForeground(Color.white);
        pause.setBackground(Color.red);
        jpCen.add(pause);
        jpCen.add(play, "gapleft 14,split 3");//divide espacio ;V    //define um espaço no valor especificado em pixels ao lado esquerdo do componente;
        jpCen.add(stop, "");
        jpCen.add(pause, "wrap");
        jpCen.add(new JSeparator(), "gapleft 14,growx, gapbottom 9");
        JPanel jpDer = new JPanel(new MigLayout("", "[right]"));
        jpDer.setBackground(Color.white);
        jpDer.setBorder(BorderFactory.createTitledBorder("Tabla de Estados"));
        jpprincipalNort.add(jpDer, "span 1 2,wrap,growx,growy");
        Object Name[] = {"Indice", "Estado", "Faltantes por Comer"};
        Object Data[][] = {{"", "", ""}, {"", "", ""}, {"", "", ""}, {"", "", ""}, {"", "", ""}, {"", "", ""}, {"", "", ""}, {"", "", ""}, {"", "", ""}, {"", "", ""}};
        jtEstados = new JTable(Data, Name);
        jtEstados.setPreferredScrollableViewportSize(new Dimension(300, 160));
        DefaultTableCellRenderer modelocentrar = new DefaultTableCellRenderer();
        modelocentrar.setHorizontalAlignment(SwingConstants.CENTER);
        jtEstados.getColumnModel().getColumn(0).setCellRenderer(modelocentrar);
        jtEstados.getColumnModel().getColumn(1).setCellRenderer(modelocentrar);
        jtEstados.getColumnModel().getColumn(2).setCellRenderer(modelocentrar);
        JScrollPane jsTabla = new JScrollPane(jtEstados);
        jsTabla.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        jpDer.add(jsTabla);
        JPanel jpimneges = new JPanel(new MigLayout("", "[]26[]26[]26[]26[]26[]", "[][][]"));
        jpimneges.setBackground(Color.white);
        jpimneges.setBorder(BorderFactory.createTitledBorder("Iconos Del Software"));
        jpprincipalNort.add(jpimneges, "span 2,wrap,align left,growy");
        ImageIcon imagenes[] = {new ImageIcon("Comida/filosofo.jpg"), new ImageIcon("Comida/tenedor.png"), new ImageIcon("Comida/pensando.png"), new ImageIcon("Comida/eat.jpg"), new ImageIcon("Comida/esperando.png"), new ImageIcon("Comida/saciado.png")};
        jpimneges.add(new JLabel(imagenes[0]), "center,cell 0 0");
        jpimneges.add(new JSeparator(), "cell 0 1,growx");
        jpimneges.add(new JLabel("-Filósofo-"), "cell 0 2,center");
        jpimneges.add(new JLabel(imagenes[1]), "center,cell 1 0");
        jpimneges.add(new JSeparator(), "cell 1 1,growx");
        jpimneges.add(new JLabel("-Tenedor-"), "cell 1 2,center");
        jpimneges.add(new JLabel(imagenes[2]), "center,cell 2 0");
        jpimneges.add(new JSeparator(), "cell 2 1,growx");
        jpimneges.add(new JLabel("-Pensar-"), "cell 2 2,center");
        jpimneges.add(new JLabel(imagenes[3]), "center,cell 3 0");
        jpimneges.add(new JSeparator(), "cell 3 1,growx");
        jpimneges.add(new JLabel("-Comer-"), "cell 3 2,center");
        jpimneges.add(new JLabel(imagenes[4]), "center,cell 4 0");
        jpimneges.add(new JSeparator(), "cell 4 1,growx");
        jpimneges.add(new JLabel("-Esperar-"), "cell 4 2,center");
        jpimneges.add(new JLabel(imagenes[5]), "center,cell 5 0");
        jpimneges.add(new JSeparator(), "cell 5 1,growx");//redimencionas
        jpimneges.add(new JLabel("-Saciado-"), "cell 5 2,center");
        JPanel jpprogreso = new JPanel(new MigLayout("", "[right]"));
        jpprogreso.setBackground(Color.WHITE);
        jpprincipalNort.add(jpprogreso, "span 3");//espacios
        final JProgressBar progreso = new JProgressBar();
        progreso.setBackground(Color.green);
        progreso.setForeground(Color.white);
        progreso.setOrientation(JProgressBar.HORIZONTAL);
        progreso.setMinimum(0);
        progreso.setMaximum(100);
        progreso.setStringPainted(true);
        progreso.setPreferredSize(new Dimension(800, 10));
        jpprogreso.add(progreso);
        if (jcTipo.getSelectedIndex() == 0) {
            cenar = new Comedor(this, Integer.valueOf(jcNfilosofos.getSelectedItem().toString()), "m");//Filosofos modernos                                
        } else {
            cenar = new Comedor(this, Integer.valueOf(jcNfilosofos.getSelectedItem().toString()), "a");//Filosofos modernos                                    
        }
        final int total = cenar.GetTotalPlatillos();
        trProgreso = new Thread(new Runnable() {
            @Override
            public void run() {
                int value = 0, nuevo = 0, porcentaje;
                while (nuevo != total) {
                    nuevo = total - cenar.GetTotalPlatillos();
                    porcentaje = nuevo * 100 / total;
                    for (int i = value; i <= porcentaje; i++) {
                        value = value + 1;
                        progreso.setValue(value);
                        try {
                            java.lang.Thread.sleep(1000);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(Ventana.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
                System.out.print("Termino");
                play.setEnabled(false);
                stop.setEnabled(false);
                pause.setEnabled(false);
                trProgreso.suspend();
                cenar.Parar();
                cenar.timer.cancel();
            }
        });

        JPanel jpfondologo = new JPanel(new MigLayout("width 0"));
        jpfondologo.setBackground(Color.black);
        JPanel jplogo = new JPanel(new MigLayout("center,width 400"));
        jplogo.setBackground(Color.white);
        JLabel creditos[] = {new JLabel(new ImageIcon("Comida/udla.png")), new JLabel("Universidad de la Amazonia"), new JLabel("Facultad de Ingeniería de Sistemas"), new JLabel("Pogramación Orientada a Eventos"), new JLabel("Cena de los Filósofos By Julio A. Vásquez L."), new JLabel(new ImageIcon("Comida/julio.png"))};
        jplogo.add(creditos[0], "center,wrap");
        jplogo.add(new JLabel(), "height 25,center,wrap");
        jplogo.add(creditos[1], "center,wrap");
        jplogo.add(creditos[2], "center,wrap");
        jplogo.add(creditos[3], "center,wrap");
        jplogo.add(creditos[4], "center,wrap");
        jplogo.add(new JLabel(), "height 25,center,wrap");
        jplogo.add(creditos[5], "center,wrap"); //ajuste al panel
        super.add(jpprincipalNort, BorderLayout.NORTH);
        super.getContentPane().add(cenar, BorderLayout.CENTER);
        super.getContentPane().add(jpfondologo, BorderLayout.EAST);
        super.getContentPane().add(jplogo, BorderLayout.WEST);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (((JButton) e.getSource()).getText().equals("Play")) {
            if (!trProgreso.isAlive()) {
                cenar.Actualizar();
                cenar.Comensar();
                trProgreso.start();
            } else {
                cenar.Retomar();
                trProgreso.resume();
            }
            play.setEnabled(false);
            pause.setEnabled(true);
            stop.setEnabled(true);
        }
        if (((JButton) e.getSource()).getText().equals("Pause")) {
            trProgreso.suspend();
            try {
                cenar.Pausar();
            } catch (InterruptedException ex) {
                Logger.getLogger(Ventana.class.getName()).log(Level.SEVERE, null, ex);
            }
            play.setEnabled(true);
            pause.setEnabled(false);
            stop.setEnabled(true);
        }
        if (((JButton) e.getSource()).getText().equals("Stop")) {
            trProgreso.suspend();
            cenar.Parar();
            play.setEnabled(true);
            pause.setEnabled(false);
            stop.setEnabled(false);
        }
    }

    public void icono() {
        Image imagen = Toolkit.getDefaultToolkit().getImage(("Comida/filosofo1.png"));
        setIconImage(imagen);
    }
}
