package Base;

import javax.swing.ImageIcon;

/* @author Julio A. Vásquez L. */
public class Tenedor {

    private int id;
    private boolean estado;      //true = esta disponible  ; false = esta ocupado     
    Filosofo cola[];
    ImageIcon imagen;
    int coordenadas[];

    public boolean getestado() {//estado deltenedr
        return estado;
    }

    public void setestado(boolean estado) {
        this.estado = estado;
    }

    public Tenedor(int id) {
        this.id = id;
        estado = true;
        cola = new Filosofo[2];
        cola[0] = null;
        cola[1] = null;
        coordenadas = new int[2];
    }

    public Filosofo[] GetEstado() {
        return cola;
    }

    public int GetId() {
        return id;
    }

    public void ponerencola(Filosofo f) {
        if (this.cola[0] != f && this.cola[1] != f) {
            if (this.cola[0] == null) {
                cola[0] = f;
            } else {
                cola[1] = f;
            }
        }
    }

    public void quitardecola() {
        this.cola[0] = this.cola[1];
        this.cola[1] = null;
    }
}
