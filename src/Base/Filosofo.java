package Base;

import javax.swing.ImageIcon;

/*@author Julio A. Vásquez L.*/
public class Filosofo extends Thread {

    int id;
    String nombre; // Nombre del filosofo -> consola
    ImageIcon imagen;
    ImageIcon imagenEstado;

    private ImageIcon tenedoresdoble;
    String estado; // Estado del Filosofo (pensando,esperando,comiendo)    
    int platillos;
    Tenedor izq; //  Izquierdo
    Tenedor der; //  Derecho
    int tiempo; // Tiempo entre pasos
    int coordenadas[]; // (x , y)
    int coordenadasEstados[]; // (x , y)
    int coordenadasCmd[]; // (x , y)

    //Aux
    ImageIcon estadoImg[] = {new ImageIcon("Comida/pensando.png"), new ImageIcon("Comida/eat.jpg"), new ImageIcon("Comida/esperando.png"), new ImageIcon("Comida/saciado.png")};

    public Filosofo(int id, String nombre, Tenedor izq, Tenedor der) {
        this.id = id;
        this.nombre = nombre;
        estado = "pensando";
        imagenEstado = estadoImg[0];
        platillos = (int) (Math.random() * 5 + 5);
        this.izq = izq;
        this.der = der;
        tiempo = (int) (Math.random() * 7 + 3) * 1000;
        coordenadas = new int[2];
        coordenadasEstados = new int[2];
        coordenadasCmd = new int[2];
    }

    @Override
    public void run() {
        try {
            System.out.println(nombre + " " + tiempo);
            Pensar();
        } catch (InterruptedException ex) {
        }
    }

    public ImageIcon GetImgagen() {
        return imagen;
    }

    public String GetEstado() {
        return estado;
    }

    public synchronized void Pensar() throws InterruptedException {
        System.out.println(nombre + " pensando...");
        estado = "pensando";
        imagenEstado = estadoImg[0];
        sleep(tiempo); // Tiempo en que piensa en filosofo 
        Comer();
    }

    public synchronized void Comer() throws InterruptedException {
        System.out.println(nombre + " Intentando Comer...");
        while (!TomarTenedorIzq() || !TomarTenedorDer()) {
            Esperar();
        }
        izq.ponerencola(this);
        der.ponerencola(this);
        if (izq.cola[0] == this && der.cola[0] == this) {
            this.izq.quitardecola();
            this.der.quitardecola();
            izq.setestado(false);
            der.setestado(false);
            System.out.println(nombre + " comiendo...");
            estado = "Comiendo";
            imagenEstado = estadoImg[1];
            sleep(tiempo); // Tiempo en el que come el filosofo
            platillos--;
            SoltarTenedores();
            if (platillos == 0) {
                Saciado();
            } else {
                Pensar();
            }
        } else {
            Comer();
        }
    }

    public synchronized void Esperar() throws InterruptedException {
        estado = "esperando";
        imagenEstado = estadoImg[2];
    }

    public synchronized void Saciado() {
        estado = "saciado";
        imagenEstado = estadoImg[3];
    }

    public synchronized boolean TomarTenedores() {
        while (TomarTenedorIzq() == true) {
        }
        while (TomarTenedorDer() == true) {
        }
        return true;
    }

    public synchronized boolean TomarTenedorIzq() {
        return (izq.getestado() == true) ? true : false;
    }

    public synchronized boolean TomarTenedorDer() {
        return (der.getestado() == true) ? true : false;
    }

    public synchronized void SoltarTenedores() {
        izq.setestado(true);
        der.setestado(true);
    }

    public void setTenedoresdoble(ImageIcon m) {
        tenedoresdoble = m;
    }

    public ImageIcon getTenedoresdoble() {
        return tenedoresdoble;
    }
}
