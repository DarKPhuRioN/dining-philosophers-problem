package Base;

import filosofosambrientos.Ventana;
import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.ImageIcon;

/*@author Julio A. Vásquez L. */
public class Comedor extends Applet {

    private final Ventana windows;
    private final String tipo;
    private int totPlatillos;
    private Filosofo Filosofo[];
    private Tenedor Tenedor[];
    private final int Cantidad;
    public Timer timer = new Timer();
    private TimerTask actualizar;

    public Comedor(Ventana vent, int cant, String t) {
        windows = vent;       // Interfas Swing           
        tipo = t;           // Antiguos o Modernos
        totPlatillos = 0;   // Total Platillos
        Cantidad = cant;       // Cantidad de Filosofos  
        Iniciar();
    }

    @Override
    public void paint(Graphics g) {
        ImageIcon ImageComedor = new ImageIcon("Comida/Comedor.png");
        ImageComedor.paintIcon(this, this.getGraphics(), 0, 0);
        //TENEDOR
        for (int i = 0; i < 5; i++) {
            Tenedor[i].imagen.paintIcon(this, this.getGraphics(), Tenedor[i].coordenadas[0], Tenedor[i].coordenadas[1]);
            Filosofo[i].imagen.paintIcon(this, this.getGraphics(), Filosofo[i].coordenadas[0], Filosofo[i].coordenadas[1]);
            Filosofo[i].imagenEstado.paintIcon(this, this.getGraphics(), Filosofo[i].coordenadasEstados[0], Filosofo[i].coordenadasEstados[1]);

        }
    }

    public void Actualizar() {
        actualizar = new TimerTask() {
            @Override
            public void run() {
                Action();
            }
        };
        timer.scheduleAtFixedRate(actualizar, 0, 800);
    }

    public void Action() {
        this.getGraphics().clearRect(0, 0, 300, 300);
        ImageIcon ImageComedor = new ImageIcon("Comida/Comedor.png");
        ImageComedor.paintIcon(this, this.getGraphics(), 0, 0);

        for (int i = 0; i < Cantidad; i++) {
            windows.jtEstados.setValueAt(Filosofo[i].id + 1, i, 0);
            windows.jtEstados.setValueAt(Filosofo[i].estado, i, 1);
            windows.jtEstados.setValueAt(Filosofo[i].platillos, i, 2);
            Filosofo[i].imagen.paintIcon(this, this.getGraphics(), Filosofo[i].coordenadas[0], Filosofo[i].coordenadas[1]);
            Filosofo[i].imagenEstado.paintIcon(this, this.getGraphics(), Filosofo[i].coordenadasEstados[0], Filosofo[i].coordenadasEstados[1]);
            if (Filosofo[i].estado.equals("Comiendo")) {
                Filosofo[i].getTenedoresdoble().paintIcon(this, this.getGraphics(), Filosofo[i].coordenadasCmd[0], Filosofo[i].coordenadasCmd[1]);
            }
        }
        for (int i = 0; i < Cantidad; i++) {
            if (Tenedor[i].getestado() == true) {
                Tenedor[i].imagen.paintIcon(this, this.getGraphics(), Tenedor[i].coordenadas[0], Tenedor[i].coordenadas[1]);
            }
        }
    }

    private void Iniciar() {
        //Inician Tenedores
        super.setBackground(Color.white);
        Tenedor = new Tenedor[Cantidad];
        for (int i = 0; i < Cantidad; i++) {
            Tenedor[i] = new Tenedor(i);
        }
        switch (Cantidad) {
            case 5:
                int c = 0;
                for (int i = 0; i < Tenedor.length; i++) {//recorrido por los tenedores
                    Tenedor[i].imagen = new ImageIcon("Comida/tenedormesa_" + (c += 1) + ".png");
                }
                Tenedor[0].coordenadas[0] = 127;
                Tenedor[0].coordenadas[1] = 73;
                Tenedor[1].coordenadas[0] = 80;
                Tenedor[1].coordenadas[1] = 109;
                Tenedor[2].coordenadas[0] = 102;
                Tenedor[2].coordenadas[1] = 160;
                Tenedor[3].coordenadas[0] = 159;
                Tenedor[3].coordenadas[1] = 159;
                Tenedor[4].coordenadas[0] = 178;
                Tenedor[4].coordenadas[1] = 106;
                break;
            case 10:
                break;
            case 15:
                break;
        }
        //Inicializando Filosofos
        Filosofo = new Filosofo[Cantidad];
        for (int i = 0; i < Cantidad; i++) {
            if (i == Cantidad - 1) {
                Filosofo[i] = new Filosofo(i, "nombre " + i, Tenedor[i], Tenedor[0]);
            } else {
                Filosofo[i] = new Filosofo(i, "nombre " + i, Tenedor[i], Tenedor[i + 1]);
            }
        }
        switch (Cantidad) {
            case 5:
                Filosofo[0].imagen = new ImageIcon("Comida/img_f" + tipo + "_" + (int) (Math.random() * 9 + 1) + ".jpg");
                Filosofo[0].setTenedoresdoble(new ImageIcon("Comida/cmd_1.png"));
                Filosofo[0].coordenadas[0] = 61;// Eje X
                Filosofo[0].coordenadas[1] = 46;// Eje y
                Filosofo[0].coordenadasEstados[0] = 27;// Eje X
                Filosofo[0].coordenadasEstados[1] = 12;// Eje y   
                Filosofo[0].coordenadasCmd[0] = 91;// Eje X
                Filosofo[0].coordenadasCmd[1] = 76;// Eje y
                Filosofo[1].imagen = new ImageIcon("Comida/img_f" + tipo + "_" + (int) (Math.random() * 9 + 1) + ".jpg");
                Filosofo[1].setTenedoresdoble(new ImageIcon("Comida/cmd_2.png"));
                Filosofo[1].coordenadas[0] = 36;// Eje X
                Filosofo[1].coordenadas[1] = 151;// Eje y
                Filosofo[1].coordenadasEstados[0] = 0;// Eje X
                Filosofo[1].coordenadasEstados[1] = 190;// Eje y  
                Filosofo[1].coordenadasCmd[0] = 74;// Eje X
                Filosofo[1].coordenadasCmd[1] = 135;// Eje y
                Filosofo[2].imagen = new ImageIcon("Comida/img_f" + tipo + "_" + (int) (Math.random() * 9 + 1) + ".jpg");
                Filosofo[2].setTenedoresdoble(new ImageIcon("Comida/cmd_3.png"));
                Filosofo[2].coordenadas[0] = 129;// Eje X
                Filosofo[2].coordenadas[1] = 213;// Eje y
                Filosofo[2].coordenadasEstados[0] = 164;// Eje X
                Filosofo[2].coordenadasEstados[1] = 251;// Eje y 
                Filosofo[2].coordenadasCmd[0] = 128;// Eje X
                Filosofo[2].coordenadasCmd[1] = 170;// Eje y
                Filosofo[3].imagen = new ImageIcon("Comida/img_f" + tipo + "_" + (int) (Math.random() * 9 + 1) + ".jpg");
                Filosofo[3].setTenedoresdoble(new ImageIcon("Comida/cmd_4.png"));
                Filosofo[3].coordenadas[0] = 224;// Eje X
                Filosofo[3].coordenadas[1] = 151;// Eje y
                Filosofo[3].coordenadasEstados[0] = 256;// Eje X
                Filosofo[3].coordenadasEstados[1] = 189;// Eje y  
                Filosofo[3].coordenadasCmd[0] = 183;// Eje X
                Filosofo[3].coordenadasCmd[1] = 130;// Eje y
                Filosofo[4].imagen = new ImageIcon("Comida/img_f" + tipo + "_" + (int) (Math.random() * 9 + 1) + ".jpg");
                Filosofo[4].setTenedoresdoble(new ImageIcon("Comida/cmd_5.png"));
                Filosofo[4].coordenadas[0] = 198;// Eje X
                Filosofo[4].coordenadas[1] = 46;// Eje y       
                Filosofo[4].coordenadasEstados[0] = 223;// Eje X
                Filosofo[4].coordenadasEstados[1] = 9;// Eje y   
                Filosofo[4].coordenadasCmd[0] = 159;// Eje X
                Filosofo[4].coordenadasCmd[1] = 73;// Eje y
                break;
            case 10:
                break;
            case 15:
                break;
        }
    }

    public void Comensar() {
        for (int i = 0; i < Cantidad; i++) {
            Filosofo[i].start();
        }
    }

    public void Retomar() {
        for (int i = 0; i < Cantidad; i++) {
            Filosofo[i].resume();
        }
    }

    public void Pausar() throws InterruptedException {
        for (int i = 0; i < Cantidad; i++) {
            Filosofo[i].suspend();
        }
    }

    public void Parar() {
        //Parar Simulacion
        for (int i = 0; i < Cantidad; i++) {
            Filosofo[i].suspend();
        }
    }

    public int GetTotalPlatillos() {
        totPlatillos = 0;
        for (int i = 0; i < Cantidad; i++) {
            totPlatillos = totPlatillos + Filosofo[i].platillos;
        }
        return totPlatillos;
    }
}
